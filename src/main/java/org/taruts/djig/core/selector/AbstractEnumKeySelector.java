package org.taruts.djig.core.selector;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class AbstractEnumKeySelector<K extends Enum<K>, T> extends AbstractSelector<K, T> {

    protected AbstractEnumKeySelector(Function<T, K> getKey, Class<K> keyClass) {
        super(getKey, () -> new EnumMap<>(keyClass));
    }

    protected AbstractEnumKeySelector(Function<T, K> getKey, Supplier<Map<K, T>> mapFactory) {
        super(getKey, mapFactory);
    }
}
