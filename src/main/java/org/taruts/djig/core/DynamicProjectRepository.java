package org.taruts.djig.core;

import org.taruts.djig.configurationProperties.HostingType;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class DynamicProjectRepository {

    private final Map<String, DynamicProject> dynamicProjectsMap = Collections.synchronizedMap(new HashMap<>());

    public DynamicProject getProject(String name) {
        return dynamicProjectsMap.get(name);
    }

    public DynamicProject getProjectRequired(String name) {
        DynamicProject project = getProject(name);
        if (project == null) {
            throw new NullPointerException("Project %s not found".formatted(name));
        }
        return project;
    }

    public void forEachProject(Consumer<DynamicProject> useDynamicProject) {
        dynamicProjectsMap.values().forEach(useDynamicProject);
    }

    public void addProject(DynamicProject dynamicProject) {
        String dynamicProjectName = dynamicProject.getName();
        if (dynamicProjectsMap.containsKey(dynamicProjectName)) {
            throw new IllegalArgumentException(String.format("A project with the name [%s] already exists", dynamicProjectName));
        }
        dynamicProjectsMap.put(dynamicProjectName, dynamicProject);
    }

    public Map<String, DynamicProject> all() {
        return Collections.unmodifiableMap(dynamicProjectsMap);
    }

    public Collection<DynamicProject> allProjects() {
        return Collections.unmodifiableCollection(dynamicProjectsMap.values());
    }

    public Stream<DynamicProject> byHostingType(HostingType hostingType) {
        return allProjects()
                .stream()
                .filter(dynamicProject ->
                        dynamicProject.getRemote().hostingType() == hostingType
                );
    }
}
