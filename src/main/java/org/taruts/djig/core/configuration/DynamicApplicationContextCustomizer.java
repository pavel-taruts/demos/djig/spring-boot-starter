package org.taruts.djig.core.configuration;

import org.taruts.djig.core.childContext.context.DynamicApplicationContext;

public interface DynamicApplicationContextCustomizer {

    void customize(DynamicApplicationContext dynamicApplicationContext);
}
