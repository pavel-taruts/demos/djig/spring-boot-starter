package org.taruts.djig.core.configuration;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.task.TaskExecutionAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.configurationProperties.DjigPropertiesAutoConfiguration;
import org.taruts.djig.core.DynamicProjectManager;
import org.taruts.djig.core.DynamicProjectRepository;
import org.taruts.djig.core.childContext.DynamicProjectContextManager;
import org.taruts.djig.core.childContext.builds.BuildService;
import org.taruts.djig.core.childContext.builds.buildableDynamicProject.BuildableDynamicProjectAutoDetector;
import org.taruts.djig.core.childContext.builds.buildableDynamicProject.BuildableDynamicProjectProvider;
import org.taruts.djig.core.childContext.builds.systems.BuildSystemsConfiguration;
import org.taruts.djig.core.childContext.remote.BaseHookUriProvider;
import org.taruts.djig.core.childContext.remote.CloneRetryTemplate;
import org.taruts.djig.core.childContext.remote.DynamicProjectCloner;
import org.taruts.djig.core.childContext.remote.WebHookRegistrar;
import org.taruts.djig.core.childContext.remote.webHookManager.hub.HubWebHookManager;
import org.taruts.djig.core.childContext.remote.webHookManager.lab.LabWebHookManager;
import org.taruts.djig.core.configurationProperties.DynamicProjectConfigurationPropertiesMapper;
import org.taruts.djig.core.configurationProperties.DynamicProjectLoaderFromConfigurationProperties;
import org.taruts.djig.core.controller.dynamicProject.DynamicProjectController;
import org.taruts.djig.core.controller.dynamicProject.DynamicProjectDtoMapper;
import org.taruts.djig.core.controller.refresh.GenericRefreshController;
import org.taruts.djig.core.controller.refresh.hub.GitHubPushEventCallbackController;
import org.taruts.djig.core.mainContext.proxy.DynamicComponentProxyRegistrar;
import org.taruts.djig.core.runtime.DynamicImplementationSelector;
import org.taruts.djig.core.webhook.hub.GitHubPushEventCallbackService;

@AutoConfiguration
@AutoConfigureAfter({DjigPropertiesAutoConfiguration.class, TaskExecutionAutoConfiguration.class})
@EnableConfigurationProperties
@Import({BuildSystemsConfiguration.class, ServletConfiguration.class, ReactiveConfiguration.class})
public class DjigAutoConfiguration {

    @Bean
    @ConfigurationProperties(DjigConfigurationProperties.PREFIX)
    public DjigConfigurationProperties djigConfigurationProperties() {
        return new DjigConfigurationProperties();
    }

    @Bean
    DynamicProjectRepository dynamicProjectRepository() {
        return new DynamicProjectRepository();
    }

    @Bean
    DynamicProjectManager dynamicProjectManager() {
        return new DynamicProjectManager();
    }

    @Bean
    DynamicProjectLoaderFromConfigurationProperties dynamicProjectLoaderFromConfigurationProperties() {
        return new DynamicProjectLoaderFromConfigurationProperties();
    }

    @Bean
    BuildableDynamicProjectAutoDetector buildTypeDetector() {
        return new BuildableDynamicProjectAutoDetector();
    }

    @Bean
    BuildService buildService() {
        return new BuildService();
    }

    @Bean
    CloneRetryTemplate cloneRetryTemplate() {
        return new CloneRetryTemplate();
    }

    @Bean
    DynamicProjectCloner dynamicProjectCloner() {
        return new DynamicProjectCloner();
    }

    @Bean
    @ConditionalOnProperty("djig.hook.host")
    WebHookRegistrar gitlabHookRegistrar() {
        return new WebHookRegistrar();
    }

    @Bean
    DynamicProjectContextManager dynamicProjectContextManager() {
        return new DynamicProjectContextManager();
    }

    @Bean
    DynamicComponentProxyRegistrar dynamicComponentProxyRegistrar() {
        return new DynamicComponentProxyRegistrar();
    }

    @Bean
    DynamicImplementationSelector dynamicImplementationSelector() {
        return new DynamicImplementationSelector();
    }

    @Bean
    GenericRefreshController genericRefreshController() {
        return new GenericRefreshController();
    }

    @Bean
    GitHubPushEventCallbackService gitHubPushEventCallbackService() {
        return new GitHubPushEventCallbackService();
    }

    @Bean
    GitHubPushEventCallbackController gitHubWebHookController() {
        return new GitHubPushEventCallbackController();
    }

    @Bean
    DynamicProjectController dynamicProjectController() {
        return new DynamicProjectController();
    }

    @Bean
    DynamicProjectConfigurationPropertiesMapper dynamicProjectConfigurationPropertiesMapper(DjigConfigurationProperties djigConfigurationProperties) {
        return new DynamicProjectConfigurationPropertiesMapper(djigConfigurationProperties);
    }

    @Bean
    BuildableDynamicProjectProvider buildableDynamicProjectProvider() {
        return new BuildableDynamicProjectProvider();
    }

    @Bean
    DynamicProjectDtoMapper dynamicProjectDtoMapper() {
        return new DynamicProjectDtoMapper();
    }

    @Bean
    LabWebHookManager labWebhookManager() {
        return new LabWebHookManager();
    }

    @Bean
    HubWebHookManager hubWebhookManager() {
        return new HubWebHookManager();
    }

    @Bean
    BaseHookUriProvider baseHookUriProvider() {
        return new BaseHookUriProvider();
    }
}
