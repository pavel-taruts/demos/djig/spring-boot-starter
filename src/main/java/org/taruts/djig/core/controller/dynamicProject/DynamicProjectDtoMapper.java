package org.taruts.djig.core.controller.dynamicProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.configurationProperties.DynamicProjectConfigurationPropertiesMapper;

public class DynamicProjectDtoMapper {

    @Autowired
    DynamicProjectConfigurationPropertiesMapper dynamicProjectConfigurationPropertiesMapper;

    public DynamicProject map(String projectName, DynamicProjectDto dto) {
        DjigConfigurationProperties.DynamicProject dynamicProjectConfigurationProperties = mapToDynamicProjectConfigurationProperties(dto);
        return dynamicProjectConfigurationPropertiesMapper.map(projectName, dynamicProjectConfigurationProperties);
    }

    private static DjigConfigurationProperties.DynamicProject mapToDynamicProjectConfigurationProperties(DynamicProjectDto dto) {
        DjigConfigurationProperties.DynamicProject target = new DjigConfigurationProperties.DynamicProject();
        target.setUrl(dto.url());
        target.setUsername(dto.username());
        target.setPassword(dto.password());
        target.setBranch(dto.branch());
        target.setBuildType(dto.buildType());
        target.setDynamicInterfacePackage(dto.dynamicInterfacePackage());
        target.setHostingType(dto.hostingType());
        return target;
    }
}
