package org.taruts.djig.core.controller.dynamicProject;

import org.taruts.djig.configurationProperties.BuildType;
import org.taruts.djig.configurationProperties.HostingType;

public record DynamicProjectDto(
        String url,
        String username,
        String password,
        String dynamicInterfacePackage,
        BuildType buildType,
        String branch,
        HostingType hostingType
) {
}
