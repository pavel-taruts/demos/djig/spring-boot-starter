package org.taruts.djig.core.controller.dynamicProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.DynamicProjectManager;

@RequestMapping("#{@djigConfigurationProperties.controller.dynamicProject.path}")
@ResponseBody
public class DynamicProjectController {

    @Autowired
    private DynamicProjectManager dynamicProjectManager;

    @Autowired
    private DynamicProjectDtoMapper dynamicProjectDtoMapper;

    @PutMapping("{projectName}")
    public void put(@PathVariable("projectName") String projectName, @RequestBody DynamicProjectDto dto) {
        DynamicProject dynamicProject = dynamicProjectDtoMapper.map(projectName, dto);
        dynamicProjectManager.addProject(dynamicProject);
    }
}
