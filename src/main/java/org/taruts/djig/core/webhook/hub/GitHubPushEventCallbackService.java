package org.taruts.djig.core.webhook.hub;

import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.DynamicProjectRepository;
import org.taruts.djig.core.childContext.DynamicProjectContextManager;
import org.taruts.djig.core.childContext.remote.DynamicProjectGitRemote;

import java.util.Set;

public class GitHubPushEventCallbackService {

    @Autowired
    private DynamicProjectRepository dynamicProjectRepository;

    @Autowired
    private DynamicProjectContextManager dynamicProjectContextManager;

    public void refreshProjects(Set<String> hookUrls, String hookBranch) {
        dynamicProjectRepository
                .allProjects()
                .stream()
                .filter(project -> projectMatches(project, hookUrls, hookBranch))
                .forEach(dynamicProjectContextManager::refresh);
    }

    private static boolean projectMatches(DynamicProject project, Set<String> hookUrls, String hookBranch) {
        DynamicProjectGitRemote remote = project.getRemote();
        String remoteUrl = remote.url();
        String remoteBranch = remote.branch();
        return hookUrls.contains(remoteUrl) && remoteBranch.equals(hookBranch);
    }
}
