package org.taruts.djig.core.childContext.builds;

import java.io.File;
import java.util.List;

public record DynamicProjectBuild(List<File> classesDirectories, List<File> resourcesDirectories, List<File> dynamicDependencies) {
}
