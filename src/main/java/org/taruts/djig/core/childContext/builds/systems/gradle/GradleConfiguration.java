package org.taruts.djig.core.childContext.builds.systems.gradle;

import org.springframework.context.annotation.Bean;

public class GradleConfiguration {

    @Bean
    GradleDynamicDependenciesProvider gradleDynamicDependenciesProvider() {
        return new GradleDynamicDependenciesProvider();
    }

    @Bean
    GradleService gradleService() {
        return new GradleService();
    }
}
