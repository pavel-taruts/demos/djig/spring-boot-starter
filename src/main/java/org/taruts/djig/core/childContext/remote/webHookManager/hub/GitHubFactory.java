package org.taruts.djig.core.childContext.remote.webHookManager.hub;

import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;

public class GitHubFactory {

    /**
     * <p>
     * Gets an authenticated GitHub API by credentials.
     * </p>
     * <p>
     * If both {@code username} and {@code password} are specified, then the login-password authentication is used.
     * </p>
     * <p>
     * If there is only {@code username} then it's expected to be a personal access token (PAT).
     * </p>
     * <p>
     * In theory, it should also work with a OAuth2 token in {@code username}, but we don't use OAuth2 in this project.
     * </p>
     */
    @SneakyThrows
    public static GitHub getGitHub(String username, String password) {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("username must be specified");
        }
        return StringUtils.isBlank(password)
                ? new GitHubBuilder().withOAuthToken(username).build()
                : new GitHubBuilder().withPassword(username, password).build();
    }
}
