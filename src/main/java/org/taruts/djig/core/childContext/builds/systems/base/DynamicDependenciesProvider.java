package org.taruts.djig.core.childContext.builds.systems.base;

import org.taruts.djig.core.childContext.builds.buildableDynamicProject.BuildableDynamicProject;

import java.io.File;
import java.util.List;

public interface DynamicDependenciesProvider {

    List<File> get(BuildableDynamicProject buildableDynamicProject);
}
