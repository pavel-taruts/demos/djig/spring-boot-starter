package org.taruts.djig.core.childContext.builds.systems;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.taruts.djig.core.childContext.builds.systems.gradle.GradleConfiguration;
import org.taruts.djig.core.childContext.builds.systems.maven.MavenConfiguration;

@Import({GradleConfiguration.class, MavenConfiguration.class})
public class BuildSystemsConfiguration {

    @Bean
    BuildSystemServiceSelector buildSystemSelector() {
        return new BuildSystemServiceSelector();
    }
}
