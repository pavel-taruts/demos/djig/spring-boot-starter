package org.taruts.djig.core.childContext.builds.systems.base;

import org.taruts.djig.configurationProperties.BuildSystem;

import java.util.List;

public interface BuildSystemService {

    BuildSystem getBuildSystem();

    DynamicDependenciesProvider getDynamicDependenciesProvider();

    List<String> getFileNames();

    String getExecutableName();

    String getWrapperScriptName();

    List<String> getParameters();

    List<String> getClassesDirectories();

    List<String> getResourceDirectories();
}
