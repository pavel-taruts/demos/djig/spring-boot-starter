package org.taruts.djig.core.childContext.context.reactive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.http.server.PathContainer;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.AbstractHandlerMapping;
import org.springframework.web.server.ServerWebExchange;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.DynamicProjectRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DjigProxyHandlerMapping extends AbstractHandlerMapping {

    @Autowired
    private DynamicProjectRepository dynamicProjectRepository;

    @Autowired
    private DjigConfigurationProperties djigConfigurationProperties;

    private String dynamicProjectControllerPathStr;

    private PathContainer dynamicProjectControllerPath;

    public DjigProxyHandlerMapping(int order) {
        super();
        setOrder(order);
    }

    @PostConstruct
    public void init() {
        String dynamicProjectControllerPathStr = djigConfigurationProperties.getController().getDynamicProject().getPath()
                .replaceAll("^/", "")
                .replaceAll("/$", "");
        dynamicProjectControllerPathStr = "/" + dynamicProjectControllerPathStr;
        this.dynamicProjectControllerPathStr = dynamicProjectControllerPathStr;

        this.dynamicProjectControllerPath = PathContainer.parsePath(dynamicProjectControllerPathStr);
    }

    @Override
    protected Mono<?> getHandlerInternal(ServerWebExchange exchange) {

        ServerHttpRequest request = exchange.getRequest();
        RequestPath requestPath = request.getPath();

        PathContainer contextPath = requestPath.contextPath();

        PathContainer pathWithinApplication = requestPath.pathWithinApplication();
        String pathWithinApplicationStr = pathWithinApplication.value();

        if (!pathWithinApplicationStr.startsWith(dynamicProjectControllerPathStr)) {
            return Mono.empty();
        }

        if (pathWithinApplicationStr.equals(dynamicProjectControllerPathStr)) {
            return Mono.empty();
        }

        // Next in a path after the prefix must go a project name
        int prefixSize = dynamicProjectControllerPath.elements().size();
        String projectName = pathWithinApplication.subPath(prefixSize + 1).elements().get(0).value();

        DynamicProject dynamicProject = dynamicProjectRepository.getProject(projectName);
        if (dynamicProject == null) {
            return Mono.empty();
        }

        // The project name is transferred from the path within application to the context path
        String modifiedContextPath = contextPath.value() + dynamicProjectControllerPath + "/" + projectName;
        ServerWebExchange modifiedExchange = exchange
                .mutate()
                .request(requestBuilder -> requestBuilder
                        .contextPath(modifiedContextPath)
                )
                .build();

        Map<String, HandlerMapping> childHandlerMappingsMap = dynamicProject.getContext().getBeansOfType(HandlerMapping.class);
        List<HandlerMapping> childHandlerMappings = new ArrayList<>(childHandlerMappingsMap.values());
        AnnotationAwareOrderComparator.sort(childHandlerMappings);

        Mono<Object> handlerMono = Flux
                .fromIterable(childHandlerMappings)
                .concatMap(childHandlerMapping -> {
                    Mono<Object> childHandlerMono = childHandlerMapping.getHandler(modifiedExchange);
                    return childHandlerMono;
                })
                .filter(Objects::nonNull)
                .take(1)
                .singleOrEmpty();

        return handlerMono;
    }
}
