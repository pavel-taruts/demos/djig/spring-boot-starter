package org.taruts.djig.core.childContext.builds.systems.maven;

import org.taruts.djig.core.childContext.builds.buildableDynamicProject.BuildableDynamicProject;
import org.taruts.djig.core.childContext.builds.systems.base.DynamicDependenciesProvider;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class MavenDynamicDependenciesProvider implements DynamicDependenciesProvider {

    @Override
    public List<File> get(BuildableDynamicProject buildableDynamicProject) {

        String output = buildableDynamicProject.runBuildScript(
                "--batch-mode",
                "dependency:build-classpath",
                "-DexcludeScope=provided"
        );

        String classpathStr = output
                .lines()
                .filter(line -> !line.startsWith("[INFO]"))
                .findFirst()
                .orElse("");

        return Arrays
                .stream(classpathStr.split(";"))
                .map(File::new)
                .toList();
    }
}
