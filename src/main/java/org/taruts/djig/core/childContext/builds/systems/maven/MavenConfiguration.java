package org.taruts.djig.core.childContext.builds.systems.maven;

import org.springframework.context.annotation.Bean;

public class MavenConfiguration {

    @Bean
    MavenDynamicDependenciesProvider mavenDynamicDependenciesProvider() {
        return new MavenDynamicDependenciesProvider();
    }

    @Bean
    MavenService mavenService() {
        return new MavenService();
    }
}
