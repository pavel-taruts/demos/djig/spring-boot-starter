package org.taruts.djig.core.childContext.builds.systems.gradle;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.configurationProperties.BuildSystem;
import org.taruts.djig.core.childContext.builds.systems.base.BuildSystemService;

import java.util.List;

public class GradleService implements BuildSystemService {

    @Autowired
    @Getter
    GradleDynamicDependenciesProvider dynamicDependenciesProvider;

    @Override
    public BuildSystem getBuildSystem() {
        return BuildSystem.GRADLE;
    }

    @Override
    public List<String> getFileNames() {
        return List.of("settings.gradle", "settings.gradle.kts", "build.gradle", "build.gradle.kts");
    }

    @Override
    public String getExecutableName() {
        return "gradle";
    }

    @Override
    public String getWrapperScriptName() {
        return "gradlew";
    }

    @Override
    public List<String> getParameters() {
        return List.of("classes");
    }

    @Override
    public List<String> getClassesDirectories() {
        return List.of("build/classes/java/main", "build/classes/kotlin/main");
    }

    @Override
    public List<String> getResourceDirectories() {
        return List.of("build/resources/main");
    }
}
