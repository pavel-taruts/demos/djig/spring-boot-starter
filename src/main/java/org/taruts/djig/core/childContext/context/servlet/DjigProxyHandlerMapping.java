package org.taruts.djig.core.childContext.context.servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.http.server.PathContainer;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.AbstractHandlerMapping;
import org.taruts.djig.configurationProperties.DjigConfigurationProperties;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.DynamicProjectRepository;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DjigProxyHandlerMapping extends AbstractHandlerMapping {

    @Autowired
    private DynamicProjectRepository dynamicProjectRepository;

    @Autowired
    private DjigConfigurationProperties djigConfigurationProperties;

    private String dynamicProjectControllerPathStr;

    private PathContainer dynamicProjectControllerPath;

    public DjigProxyHandlerMapping(int order) {
        super();
        setOrder(order);
    }

    @PostConstruct
    public void init() {
        String dynamicProjectControllerPathStr = djigConfigurationProperties.getController().getDynamicProject().getPath()
                .replaceAll("^/", "")
                .replaceAll("/$", "");
        dynamicProjectControllerPathStr = "/" + dynamicProjectControllerPathStr;
        this.dynamicProjectControllerPathStr = dynamicProjectControllerPathStr;

        this.dynamicProjectControllerPath = PathContainer.parsePath(dynamicProjectControllerPathStr);
    }

    @Override
    protected Object getHandlerInternal(HttpServletRequest request) throws Exception {

        String contextPathStr = request.getContextPath();

        String pathWithinApplicationStr = request.getServletPath();
        PathContainer pathWithinApplication = PathContainer.parsePath(pathWithinApplicationStr);

        if (!pathWithinApplicationStr.startsWith(dynamicProjectControllerPathStr)) {
            return null;
        }

        if (pathWithinApplicationStr.equals(dynamicProjectControllerPathStr)) {
            return null;
        }

        // Next in a path after the prefix must go a project name
        int prefixSize = dynamicProjectControllerPath.elements().size();
        String projectName = pathWithinApplication.subPath(prefixSize + 1).elements().get(0).value();

        DynamicProject dynamicProject = dynamicProjectRepository.getProject(projectName);
        if (dynamicProject == null) {
            return null;
        }

        // The project name is transferred from the path within application to the context path
        String modifiedContextPath = contextPathStr + dynamicProjectControllerPath + "/" + projectName;

        DjigProxyHttpServletRequestWrapper modifiedRequest = new DjigProxyHttpServletRequestWrapper(request, modifiedContextPath);

        Map<String, HandlerMapping> childHandlerMappingsMap = dynamicProject.getContext().getBeansOfType(HandlerMapping.class);
        List<HandlerMapping> childHandlerMappings = new ArrayList<>(childHandlerMappingsMap.values());
        AnnotationAwareOrderComparator.sort(childHandlerMappings);

        for (HandlerMapping childHandlerMapping : childHandlerMappings) {
            HandlerExecutionChain handlerExecutionChain = childHandlerMapping.getHandler(modifiedRequest);
            if (handlerExecutionChain != null) {
                return handlerExecutionChain;
            }
        }

        return null;
    }
}
