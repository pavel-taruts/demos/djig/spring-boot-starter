package org.taruts.djig.core.childContext.builds.buildableDynamicProject;

import org.taruts.djig.core.childContext.builds.systems.base.BuildSystemService;
import org.taruts.processUtils.ProcessRunner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public record BuildableDynamicProject(File sourceDirectory, String wrapperScriptFileNameWithExtension,
                                      BuildSystemService buildSystemService) {

    public String runBuildScript(String... parameters) {
        return runBuildScript(Arrays.asList(parameters));
    }

    public String runBuildScript(List<String> parameters) {
        if (this.isWrapper()) {
            return ProcessRunner.runScript(sourceDirectory, wrapperScriptFileNameWithExtension, parameters);
        } else {
            String executableName = buildSystemService().getExecutableName();
            return ProcessRunner.runExecutable(sourceDirectory, executableName, parameters);
        }
    }

    public boolean isWrapper() {
        return wrapperScriptFileNameWithExtension != null;
    }
}
