package org.taruts.djig.core.childContext.remote.webHookManager.lab;

import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.gitlab4j.api.GitLabApi;

public class GitLabApiFactory {

    /**
     * <p>
     * Gets an authenticated {@link GitLabApi} by credentials.
     * </p>
     * <p>
     * If both {@code username} and {@code password} are specified, then the login-password authentication is used.
     * </p>
     * <p>
     * If there is only {@code username} then it's expected to be a personal access token (PAT).
     * </p>
     */
    @SneakyThrows
    public static GitLabApi getGitLabApi(String urlStr, String username, String password) {
        if (StringUtils.isBlank(password)) {
            return new GitLabApi(urlStr, username);
        } else {
            return GitLabApi.oauth2Login(urlStr, username, password, true);
        }
    }
}
