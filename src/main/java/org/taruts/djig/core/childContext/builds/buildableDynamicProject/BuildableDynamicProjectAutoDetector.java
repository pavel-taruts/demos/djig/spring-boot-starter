package org.taruts.djig.core.childContext.builds.buildableDynamicProject;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.taruts.djig.configurationProperties.BuildSystem;
import org.taruts.djig.core.childContext.builds.systems.BuildSystemServiceSelector;
import org.taruts.djig.core.childContext.builds.systems.base.BuildSystemService;

import java.io.File;
import java.util.List;
import java.util.stream.Stream;

public class BuildableDynamicProjectAutoDetector {

    @Autowired
    BuildSystemServiceSelector buildSystemServiceSelector;

    public BuildableDynamicProject detect(File sourceDirectory) {
        return detectAll(sourceDirectory)
                .stream()
                .findFirst()
                .orElse(null);
    }

    private List<BuildableDynamicProject> detectAll(File sourceDirectory) {
        return Stream
                .of(BuildSystem.values())
                .map(buildSystemServiceSelector::select)
                .filter(buildSystemService -> isFileExists(sourceDirectory, buildSystemService))
                .map(buildSystemService -> createBuildableDynamicProject(sourceDirectory, buildSystemService))
                .toList();
    }

    private static boolean isFileExists(File sourceDirectory, BuildSystemService buildSystemService) {
        boolean fileExists = FileSearchUtils.fileExists(
                sourceDirectory,
                buildSystemService.getFileNames()
        );
        return fileExists;
    }

    private BuildableDynamicProject createBuildableDynamicProject(File sourceDirectory, BuildSystemService buildSystemService) {
        String wrapperScriptFileNameWithExtension = findWrapperScriptFileNameWithExtension(sourceDirectory, buildSystemService);
        return new BuildableDynamicProject(sourceDirectory, wrapperScriptFileNameWithExtension, buildSystemService);
    }

    public String findWrapperScriptFileNameWithExtension(File directory, BuildSystemService buildSystemService) {
        String wrapperScriptName = buildSystemService.getWrapperScriptName();
        String wrapperScriptFileNameWithExtension = SystemUtils.IS_OS_WINDOWS
                ? FileSearchUtils.findFile(directory, wrapperScriptName + ".cmd", wrapperScriptName + ".bat")
                : FileSearchUtils.findFile(directory, wrapperScriptName, wrapperScriptName + ".sh");
        return wrapperScriptFileNameWithExtension;
    }
}
